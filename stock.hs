﻿import System.Process
-------------------------------------
--   __    __   ______    ______   --
--  |  |  |  | |   _  \  |  ____|  --
--  |  |  |  | |  | |  | | |___    -- 
--  |  |  |  | |  |_|  | |  ___|   --
--  |  |__|  | |   ___/  | |____   -- 
--   \______/  |__|      |______|  --
--                                 --
-------------------------------------
-- Escola Politécnica de Pernambuco
-- Disciplína LPF 
-- Aluno: Álefe Cruz da Silva
-- O trabalho consistem num sistema de caixa de um mercado, onde são feita compras atraves do cliente.
-- O funcionário e que utiliza o sistama para fechar compras de um determinado cliente.
-- No sistema encontras todos os produtos do mercado e cada produto possui seu respectivo valor(preco) e sua quantidade.
-- Para efetuar uma compra, o funcionario deve inserir todos o nomes dos produtos, e imediatamente será calculado o valor total da compra e ,
-- decrementado as quantidades em relacao ao numero de cada produtos comprados no banco.
-- o funcionario tambem e responsável em adicionar e remove produtos no sistema com seus respectivos valores e quantidades
-- o funcionario tambem pode verificar se um determinado produto consiste no sistema e todos os produtos nele existente.

type Nome = String
type Qtd = Int
type Valor = Float
data Produto = Vazio | Pro Nome Valor Qtd deriving (Show,Read,Eq)
type Dados = [Produto]  
type Compra = [(Valor,Qtd)]

main = menu
cliente_ler_Arquivo :: IO(Compra)
cliente_ler_Arquivo = do n <- readFile "Cliente.txt"
                         readIO n

cliente_Arquivo :: Compra -> IO()
cliente_Arquivo x = writeFile "Cliente.txt" (show x)

salva_Arquivo :: Dados -> IO()
salva_Arquivo x = writeFile "DataBase.txt" (show x)

ler_Arquivo :: IO(Dados)
ler_Arquivo = do n <- readFile "DataBase.txt"
                 readIO n




func_pegavalor :: Dados -> Nome -> IO(Valor)
func_pegavalor  [Vazio] n = return 0
func_pegavalor  [] n      = return 0
func_pegavalor  ((Pro nome valor qtd):t) n | nome == n = return valor
                                           | otherwise = func_pegavalor t n

-- funcao responsavel em listar todos os produtos do banco e espera qualquer tecla para sair da funcao.
func_listar :: Dados -> IO()
func_listar [Vazio] = do putStrLn "-----------------------------------------"
                         putStrLn " Nenhum Produto Cadastrado!!! "
                         putStrLn "Digite qualquer tecla para voltar. "
                         n <- getChar
                         putStrLn " "
func_listar [] = do putStrLn "-----------------------------------------"
                    putStrLn "Digite qualquer tecla para voltar. "
                    n <- getChar
                    putStrLn " "
func_listar ((Pro nome valor qtd):t) = do putStr " "
                                          putStr("-> "++nome++" "++(show valor)++" "++(show qtd)++"\n")
                                          func_listar t

func_remover :: Nome -> Dados -> Dados
func_remover nome [Vazio] = [Vazio]
func_remover nome [] = []
func_remover nome ((Pro nomeD valorD qtdD):t) | nome == nomeD =  t
                                              | otherwise = (Pro nomeD valorD qtdD): func_remover nome t

func_verificar :: Nome -> Dados -> IO(Produto)
func_verificar nome [Vazio] = return Vazio
func_verificar nome [] = return Vazio
func_verificar nome ((Pro nomeD valorD qtdD):t) | nome == nomeD = return (Pro nomeD valorD qtdD)
                                                | otherwise = func_verificar nome t

func_inserir :: Nome -> Valor -> Qtd -> Dados -> Dados
func_inserir nome valor qtd [Vazio] = [(Pro nome valor qtd)]
func_inserir nome valor qtd [] = [(Pro nome valor qtd)]
func_inserir nome valor qtd  t  = (Pro nome valor qtd):t

func_valorTotal :: Compra -> Valor
func_valorTotal       []         = 0.00
func_valorTotal ((valor,qtd): t) = (valor*((read (show qtd))::Float)) + func_valorTotal t

func_Total :: Valor -> IO(Valor)
func_Total valor = return valor

func_AttDados :: Dados -> Nome -> Qtd -> Dados
func_AttDados  [Vazio] n q = [Vazio]
func_AttDados  []      n q = []
func_AttDados ((Pro nome valor qtd):t) n q | nome == n = (Pro nome valor (qtd-q)): t
                                           | otherwise = (Pro nome valor qtd) : func_AttDados t n q

pega_Valor :: IO(Float)
pega_Valor = do putStrLn "Digite o valor do Produto: "
                m <- getLine
                readIO m


pega_Qtd :: IO(Int)
pega_Qtd = do putStrLn "Digite a quantidade do Produto: "
              n <- getLine
              readIO n

comprar :: IO()
comprar = do system("cls")
             putStrLn "--------- Menu de Compras ---------"
             putStrLn "Digite o Nome do produto: "
             nome <- getLine
             qtd  <- pega_Qtd
             banco <- ler_Arquivo
             dadoCompra <- cliente_ler_Arquivo
             valor <- func_pegavalor banco nome
             cliente_Arquivo ((valor,qtd):dadoCompra)
             salva_Arquivo (func_AttDados banco nome qtd)
             loopCompra
           
inserir :: IO()
inserir = do system("cls")
             putStrLn "--------- Inserir Produto ---------"
             putStrLn "Digite o nome do Produto: "
             nome <- getLine
             valor <- pega_Valor
             qtd <- pega_Qtd
             banco <- ler_Arquivo
             bancoAtt <- salva_Arquivo (func_inserir nome valor qtd banco)
             putStr("-> "++nome++" "++(show valor)++" "++(show qtd)++"\n")
             putStrLn "----------------------------------"
             putStrLn "Digite qualquer tecla para voltar. "
             n <- getChar
             putStrLn " "
             admin

remover :: IO()
remover = do system("cls")
             putStrLn "--------- Remover Produto ---------"
             putStrLn "Digite O Nome do Produto: "
             nome <- getLine
             banco <- ler_Arquivo
             bancoAtt <- salva_Arquivo (func_remover nome banco)
             putStrLn "----------------------------------"
             putStrLn "Digite qualquer tecla para voltar. "
             n <- getChar
             putStrLn " "
             admin


listar :: IO()
listar = do system("cls")
            putStrLn "----------- Lista de Produtos -----------"
            putStrLn " Nome      Valor Quantidade"
            ban <- ler_Arquivo
            func_listar ban
            menu

loopCompra :: IO()
loopCompra = do putStrLn "Deseja finalizar a compra ? S - sim / N - nao ."
                p <- getLine
                loopPro p
                    where loopPro p
                             | p == "S" || p == "s" = do cliente <- cliente_ler_Arquivo
                                                         total <- func_Total(func_valorTotal cliente)
                                                         putStr("O Valor total da Compra: R$"++(show total)++"\n")
                                                         putStrLn "Digite qualquer tecla para voltar. "
                                                         n <- getChar
                                                         putStrLn "  "
                                                         menu
                             | p == "N" || p == "n" = comprar
                             | otherwise = do putStrLn "Digite uma opcao valida. "
                                              loopCompra

loop :: IO()
loop = do putStrLn "Deseja voltar para o menu ? S - sim / N - nao ."
          p <- getLine
          loopProcesso p
                where loopProcesso p
                         | p == "S" || p == "s" = menu
                         | p == "N" || p == "n" = verificar
                         | otherwise = do putStrLn "Digite uma opcao valida. "
                                          loop

verificar :: IO()
verificar = do system("cls")
               putStrLn "---------- Verificar Produto ---------"
               putStrLn "Informe o nome do Produto que deseja verifica: "
               nome <- getLine
               banco <- ler_Arquivo
               prod <- func_verificar nome banco
               if prod == Vazio then do putStrLn "Produto nao consta no sistema. "
                                        loop
                                else do putStrLn (show prod)
                                        loop
admin :: IO()
admin = do system("cls")
           putStrLn "-------- Area do Administrador --------\n"
           putStrLn " (1) - Inserir Produto "
           putStrLn " (2) - Remover Produto "
           putStrLn " (3) - Listar Produtos "
           putStrLn " (4) - Menu"
           putStr "Entre com uma opcao: "
           adminOP <- getLine
           adminProcesso adminOP
                          where adminProcesso adminOP
                                               | adminOP == "1" = inserir
                                               | adminOP == "2" = remover
                                               | adminOP == "3" = listar
                                               | adminOP == "4" = menu
                                               | otherwise = admin
menu :: IO ()
menu = do system("cls")
          putStrLn "---------------  MENU  ----------------\n"
          putStrLn " (1) - Comprar Produtos "
          putStrLn " (2) - Verificar Produtos "
          putStrLn " (3) - Listar todos os Produtos "
          putStrLn " (4) - Area do administrador "
          putStrLn " (5) - Sair"
          putStr "Entre com uma opcao: "
          opcao <- getLine
          processo opcao
                   where processo opcao
                                   | opcao == "1" = do cliente_Arquivo []
                                                       comprar
                                   | opcao == "2" = verificar
                                   | opcao == "3" = listar
                                   | opcao == "4" = admin
                                   | opcao == "5" = do return()
                                   | otherwise = menu